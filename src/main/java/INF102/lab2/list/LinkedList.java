package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;
	
	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}
	
	/**
     * Returns the node at the specified position in this list.
     *
     * @param index index of the node to return
     * @return the node at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
	private Node<T> getNode(int index) {
		//sees if the list is empty or if index is out of bounds
		if(index < 0 || index >= size()){
			throw new IndexOutOfBoundsException();
		}
		//loops trough the nodes until you are at the node you want to get
		Node<T> newNode = head;
		for(int i = 0; i < index; i++){
			newNode = newNode.next;
		}
		return newNode;
	}

	@Override
	public void add(int index, T element) {
		//same check as in getNode
		if(index <0 || index > size()){
			throw new IndexOutOfBoundsException();
		}
		//counts the amount of nodes
		n++;
		//creates a node with elemnt
		Node<T> newNode = new Node<>(element);
		//if the list is empty it msakes the new node the head node
		if(isEmpty() || index == 0){
			newNode.next = head;
			head = newNode;
			return;
		}
		//else it will check the node before and after
		Node<T> nodeBefore = getNode(index-1);
		Node<T> nodeAfter = nodeBefore.next;
		//then tell the new node what is behind and after it
		nodeBefore.next = newNode;
		newNode.next = nodeAfter;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}
	
}