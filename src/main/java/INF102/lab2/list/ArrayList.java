package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		// Checks if the list is empty and thorws exception if it is.
		if(isEmpty()){
			throw new IndexOutOfBoundsException();
		}
		//returns the elemnt form the list 
		return (T) elements[index];
	
	}
	
	@Override
	public void add(int index, T element ) {
		n++;
		if( elements.length < size()){//sees if the list is filled up yet
			Object[] newlist = new Object [elements.length + DEFAULT_CAPACITY];//if it is it creates a new bigger list
			//this loop adds the old elements to the list so we dont lose anything and adds the new element 
			for(int i = 0; i<size(); i++){
				if(i < index){
					newlist[i] = elements[i];
  				}
				if(i == index){
					newlist[i] = element;
				}
				if(i>index){
					newlist[i] = elements[i-1];
				}
			}
		//overrides the old list
		elements = newlist;
		}		
		else{
			//moves all the elemnt down to where you want to add your element one to the left then adds the new element.
			for(int i = size()-1; i >= index; i--){
				if(i == index){
					elements[i] = element;
				}
				if(i>index){
					elements[i] = elements[i-1];
				}
			}

		}

	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}